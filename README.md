# Introduction

<p>Vacation Rental is a software that provides a solution for hosts to register their rentals and assign received bookings to them. B2B customers can start to rent their apartments or rooms by leveraging the system to take care about occupation info, overbookings and so on.</p>

# The architecture

<p>The software has been architected as a 3 layered solution with a set of well-defined API exposed to our B2B customers:</p>



|     |  Web Api |     |
|-----|-----|-----|
|  RentalsController | BookingsController    | CalendarController   |

</center>

|   |  Service Layer |   |
|---|---|---|
| RentalService |   | BookingService  |

|   |   | Data Access Layer  |   |   |
|---|---|---|---|---|
| RentalRepository  | InMemoryRentalRepository   |   | BookingRepository   | InMemoryBookingRepository   |

</center>

<p>The double implementation of the repository pattern [In-Memory or Persistent] allows to easily switch, at registration time, from an in-memory solution to Entity Framework and SQL server.<br />
For the scope of this project, the application has been tested with the in-memory settings only.</p>

Some of the architectural choices I would like to mention:
 - Interface segregation
 - Repository pattern
 - SOLID approach with IoC
 - Strategy pattern to make booking policy easily extensible
 - Public interfaces and internal implementations to avoid coupling beetween layers

Both unit and integration tests have been created, the code coverage hits a percentage greater than 90%.