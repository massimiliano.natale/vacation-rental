﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using VacationRental.Api.Models;

namespace VacationRental.Api.Tests
{
    public class BaseControllerTests
    {
        protected readonly HttpClient Client;

        public BaseControllerTests(IntegrationFixture fixture)
        {
            Client = fixture.Client;
        }

        protected async Task<RentalViewModel> GetRentalByIdAsync(int id)
        {
            using (var getResponse = await Client.GetAsync($"/api/v1/rentals/{id}"))
            {
                return await getResponse.Content.ReadAsAsync<RentalViewModel>();
            }
        }

        protected async Task<ResourceIdViewModel> CreateRentalAsync(int preparationTimeInDays, int units)
        {
            var rental = new RentalBindingModel
            {
                PreparationTimeInDays = preparationTimeInDays,
                Units = units
            };

            using (var getResponse = await Client.PostAsJsonAsync($"/api/v1/rentals", rental))
            {
                return await getResponse.Content.ReadAsAsync<ResourceIdViewModel>();
            }
        }

        protected async Task<ResourceIdViewModel> UpdateRentalByIdAsync(int id, int preparationTimeInDays, int units)
        {
            var rental = new RentalBindingModel
            {
                PreparationTimeInDays = preparationTimeInDays,
                Units = units
            };

            using (var getResponse = await Client.PutAsJsonAsync($"/api/v1/rentals/{id}", rental))
            {
                return await getResponse.Content.ReadAsAsync<ResourceIdViewModel>();
            }
        }

        protected async Task<BookingViewModel> GetBookingByIdAsync(int id)
        {
            using (var getResponse = await Client.GetAsync($"/api/v1/bookings/{id}"))
            {
                return await getResponse.Content.ReadAsAsync<BookingViewModel>();
            }
        }

        protected async Task<ResourceIdViewModel> PostBookingAsync(int rentalId, DateTime start, int nights)
        {
            var postBookingRequest = new BookingBindingModel
            {
                RentalId = rentalId,
                Start = start,
                Nights = nights
            };

            using (var postBookingResponse = await Client.PostAsJsonAsync($"/api/v1/bookings", postBookingRequest))
            {
                return await postBookingResponse.Content.ReadAsAsync<ResourceIdViewModel>();
            }
        }

        protected async Task<CalendarViewModel> GetCalendarAsync(int rentalId, DateTime start, int nights)
        {
            using (var getCalendarResponse = await Client.GetAsync($"/api/v1/calendar?rentalId={rentalId}&start={start.Date:yyyy-MM-dd}&nights={nights}"))
            {
                return await getCalendarResponse.Content.ReadAsAsync<CalendarViewModel>();
            }
        }
    }
}
