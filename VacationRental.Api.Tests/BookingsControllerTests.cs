﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace VacationRental.Api.Tests
{
    [Collection("Integration")]
    public class BookingsControllerTests : BaseControllerTests
    {
        public BookingsControllerTests(IntegrationFixture fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task CreateBookings_ShouldFollowRequirements()
        {
            var bookingDatetime = new DateTime(2000, 1, 1, 17, 54, 43);
            var bookingDate = new DateTime(2000, 1, 1, 0, 0, 0);
            var rentalResourceId = await CreateRentalAsync(1, 3);

            var firstBookingResourceId = await PostBookingAsync(rentalResourceId.Id, bookingDatetime, 5);
            var firstBooking = await GetBookingByIdAsync(firstBookingResourceId.Id);

            Assert.Equal(firstBookingResourceId.Id, firstBooking.Id);
            Assert.Equal(bookingDate, firstBooking.Start);
            Assert.NotEqual(bookingDatetime, firstBooking.Start);
            Assert.Equal(5, firstBooking.Nights);
            Assert.Equal(rentalResourceId.Id, firstBooking.RentalId);

            await PostBookingAsync(rentalResourceId.Id, bookingDatetime, 5);
            await PostBookingAsync(rentalResourceId.Id, bookingDatetime, 5);

            await Assert.ThrowsAnyAsync<ApplicationException>(async () => await PostBookingAsync(rentalResourceId.Id, bookingDatetime, 5));
            await UpdateRentalByIdAsync(rentalResourceId.Id, 2, 3);
            await Assert.ThrowsAsync<ApplicationException>(async () => await PostBookingAsync(rentalResourceId.Id, bookingDatetime.AddDays(6), 5));
        }
    }
}
