﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using VacationRental.Api.Models;
using Xunit;

namespace VacationRental.Api.Tests
{
    [Collection("Integration")]
    public class CalendarControllerTests : BaseControllerTests
    {
        public CalendarControllerTests(IntegrationFixture fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task GetCalendar_ShouldFollowRequirements()
        {
            var bookingDatetime = new DateTime(2000, 1, 1, 17, 54, 43);
            var rentalResourceId = await CreateRentalAsync(3, 5);

            var bookingResourceId1 = await PostBookingAsync(rentalResourceId.Id, bookingDatetime, 5);
            var bookingResourceId2 = await PostBookingAsync(rentalResourceId.Id, bookingDatetime.AddDays(1), 5);
            var bookingResourceId3 = await PostBookingAsync(rentalResourceId.Id, bookingDatetime.AddDays(2), 5);
            var bookingResourceId4 = await PostBookingAsync(rentalResourceId.Id, bookingDatetime.AddDays(3), 5);

            var calendar = await GetCalendarAsync(rentalResourceId.Id, bookingDatetime, 12);
            var expectedBookings = GetExpectedBookings(bookingResourceId1, bookingResourceId2, bookingResourceId3, bookingResourceId4);
            var expectedPreparationTimes = GetExpectedPreparationTimes();


            Assert.Equal(rentalResourceId.Id, calendar.RentalId);
            for (var i = 0; i < 12; i++)
            {
                Assert.Equal(bookingDatetime.Date.AddDays(i), calendar.Dates[i].Date);
                calendar.Dates[i].Bookings.Should().BeEquivalentTo(expectedBookings[i]);
                calendar.Dates[i].PreparationTimes.Should().BeEquivalentTo(expectedPreparationTimes[i]);
            }
        }

        private static List<List<PreparationTimeViewModel>> GetExpectedPreparationTimes()
        {
            var expectedPreparationTimes = new List<List<PreparationTimeViewModel>>
            {
                new List<PreparationTimeViewModel>(),
                new List<PreparationTimeViewModel>(),
                new List<PreparationTimeViewModel>(),
                new List<PreparationTimeViewModel>(),
                new List<PreparationTimeViewModel>(),
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 1
                    }
                },
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 1
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 2
                    }
                },
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 1
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 2
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 3
                    }
                },
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 2
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 3
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 4
                    }
                },
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 3
                    },
                    new PreparationTimeViewModel
                    {
                        Unit = 4
                    }
                },
                new List<PreparationTimeViewModel>
                {
                    new PreparationTimeViewModel
                    {
                        Unit = 4
                    }
                },
                new List<PreparationTimeViewModel>()
            };

            return expectedPreparationTimes;
        }

        private static List<List<CalendarBookingViewModel>> GetExpectedBookings(ResourceIdViewModel bookingResourceId1, ResourceIdViewModel bookingResourceId2, ResourceIdViewModel bookingResourceId3, ResourceIdViewModel bookingResourceId4)
        {
            var expectedBookings = new List<List<CalendarBookingViewModel>>
            {
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId1.Id,
                        Unit = 1
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId1.Id,
                        Unit = 1
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId2.Id,
                        Unit = 2
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId1.Id,
                        Unit = 1
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId2.Id,
                        Unit = 2
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId3.Id,
                        Unit = 3
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId1.Id,
                        Unit = 1
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId2.Id,
                        Unit = 2
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId3.Id,
                        Unit = 3
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId4.Id,
                        Unit = 4
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId1.Id,
                        Unit = 1
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId2.Id,
                        Unit = 2
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId3.Id,
                        Unit = 3
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId4.Id,
                        Unit = 4
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId2.Id,
                        Unit = 2
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId3.Id,
                        Unit = 3
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId4.Id,
                        Unit = 4
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId3.Id,
                        Unit = 3
                    },
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId4.Id,
                        Unit = 4
                    }
                },
                new List<CalendarBookingViewModel>
                {
                    new CalendarBookingViewModel
                    {
                        Id = bookingResourceId4.Id,
                        Unit = 4
                    }
                },
                new List<CalendarBookingViewModel>(),
                new List<CalendarBookingViewModel>(),
                new List<CalendarBookingViewModel>(),
                new List<CalendarBookingViewModel>()
            };

            return expectedBookings;
        }
    }
}
