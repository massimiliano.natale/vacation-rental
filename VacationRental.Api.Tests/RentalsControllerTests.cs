﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace VacationRental.Api.Tests
{
    [Collection("Integration")]
    public class RentalsControllerTests : BaseControllerTests
    {
        public RentalsControllerTests(IntegrationFixture fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task CreateAndUpdateRentalsWithNoBooking_ShouldFollowRequirements()
        {
            var firstResourceId = await CreateRentalAsync(5, 10);
            var secondResourceId = await CreateRentalAsync(10, 1);
            var thirdResourceId = await CreateRentalAsync(20, 3);

            var firstRental = await GetRentalByIdAsync(firstResourceId.Id);
            var secondRental = await GetRentalByIdAsync(secondResourceId.Id);
            var thirdRental = await GetRentalByIdAsync(thirdResourceId.Id);

            Assert.Equal(5, firstRental.PreparationTimeInDays);
            Assert.Equal(10, firstRental.Units);
            Assert.Equal(10, secondRental.PreparationTimeInDays);
            Assert.Equal(1, secondRental.Units);
            Assert.Equal(20, thirdRental.PreparationTimeInDays);
            Assert.Equal(3, thirdRental.Units);

            secondResourceId = await UpdateRentalByIdAsync(secondRental.Id, 44, 44);

            firstRental = await GetRentalByIdAsync(firstResourceId.Id);
            secondRental = await GetRentalByIdAsync(secondResourceId.Id);
            thirdRental = await GetRentalByIdAsync(thirdResourceId.Id);

            Assert.Equal(5, firstRental.PreparationTimeInDays);
            Assert.Equal(10, firstRental.Units);
            Assert.Equal(44, secondRental.PreparationTimeInDays);
            Assert.Equal(44, secondRental.Units);
            Assert.Equal(20, thirdRental.PreparationTimeInDays);
            Assert.Equal(3, thirdRental.Units);
        }

        [Fact]
        public async Task UpdateRental_ShouldCheckPreparationTimePolicy()
        {
            var firstResourceId = await CreateRentalAsync(1, 5);

            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 13), 10);

            await UpdateRentalByIdAsync(firstResourceId.Id, 2, 5);
            var firstRental = await GetRentalByIdAsync(firstResourceId.Id);

            Assert.Equal(2, firstRental.PreparationTimeInDays);
            Assert.Equal(5, firstRental.Units);

            await Assert.ThrowsAsync<ApplicationException>(async () => await UpdateRentalByIdAsync(firstResourceId.Id, 3, 5));
        }

        [Fact]
        public async Task UpdateRental_ShouldCheckUnitsPolicy()
        {
            var firstResourceId = await CreateRentalAsync(1, 5);

            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);
            await PostBookingAsync(firstResourceId.Id, new DateTime(2000, 1, 1), 10);

            await UpdateRentalByIdAsync(firstResourceId.Id, 1, 4);
            var firstRental = await GetRentalByIdAsync(firstResourceId.Id);

            Assert.Equal(1, firstRental.PreparationTimeInDays);
            Assert.Equal(4, firstRental.Units);

            await Assert.ThrowsAsync<ApplicationException>(async () => await UpdateRentalByIdAsync(firstResourceId.Id, 3, 3));
        }
    }
}
