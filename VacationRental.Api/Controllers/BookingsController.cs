﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VacationRental.Api.Models;
using VacationRental.Business.DTO;
using VacationRental.Business.Interfaces;

namespace VacationRental.Api.Controllers
{
    [Route("api/v1/bookings")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IRentalService rentalService;
        private readonly IBookingService bookingService;

        public BookingsController(IRentalService rentalService, IBookingService bookingService)
        {
            this.rentalService = rentalService;
            this.bookingService = bookingService;
        }

        [HttpGet]
        [Route("{bookingId:int}")]
        public async Task<BookingViewModel> Get(int bookingId)
        {
            var booking = await bookingService.GetBookingInfoByIdAsync(bookingId);

            // The case when the booking is not found should be handled. I didn't do it as per requirement the contract shouldn't be updated
            // Throwing a different exception could be considered a violation to this constraint

            return new BookingViewModel
            {
                Id = booking.Id,
                RentalId = booking.RentalId,
                Start = booking.Start,
                Nights = booking.Nights
            };
        }

        [HttpPost]
        public async Task<ResourceIdViewModel> Post(BookingBindingModel model)
        {
            int bookingId;

            if (model.Nights <= 0)
                throw new ApplicationException("Nights must be positive");
            if (!await rentalService.IsARegisteredUserRentalByIdAsync(model.RentalId))
                throw new ApplicationException("Rental not found");

            try
            {
                bookingId = await bookingService.SubmitBookingForRentalAsync(new BookingDTO
                {
                    RentalId = model.RentalId,
                    Start = model.Start.Date,
                    Nights = model.Nights
                });
            }
            catch (InvalidOperationException ex)
            {
                throw new ApplicationException(ex.Message);
            }

            return new ResourceIdViewModel { Id = bookingId };
        }
    }
}
