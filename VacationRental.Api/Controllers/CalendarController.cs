﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VacationRental.Api.Models;
using VacationRental.Business.DTO;
using VacationRental.Business.Interfaces;

namespace VacationRental.Api.Controllers
{
    [Route("api/v1/calendar")]
    [ApiController]
    public class CalendarController : ControllerBase
    {
        private readonly IRentalService rentalService;
        private readonly IBookingService bookingService;

        public CalendarController(IRentalService rentalService, IBookingService bookingService)
        {
            this.rentalService = rentalService;
            this.bookingService = bookingService;
        }

        [HttpGet]
        public async Task<CalendarViewModel> Get(int rentalId, DateTime start, int nights)
        {
            if (nights < 0)
                throw new ApplicationException("Nights must be positive");

            if (!await rentalService.IsARegisteredUserRentalByIdAsync(rentalId))
                throw new ApplicationException("Rental not found");

            var rental = await rentalService.GetRegisteredUserRentalByIdAsync(rentalId);
            var bookings = await bookingService.GetBookingsByRentalIdAsync(rentalId);

            return GetPopulatedCalendarViewModel(rental, bookings, start, nights);
        }

        private static CalendarViewModel GetPopulatedCalendarViewModel(RentalDTO rental, IList<BookingDTO> bookings, DateTime start, int nights)
        {
            var result = new CalendarViewModel
            {
                RentalId = rental.Id,
                Dates = new List<CalendarDateViewModel>()
            };

            for (var i = 0; i < nights; i++)
            {
                var date = new CalendarDateViewModel
                {
                    Date = start.Date.AddDays(i),
                    Bookings = new List<CalendarBookingViewModel>(),
                    PreparationTimes = new List<PreparationTimeViewModel>()
                };

                foreach (var booking in bookings)
                {
                    if (booking.Start <= date.Date && booking.Start.AddDays(booking.Nights) > date.Date)
                    {
                        date.Bookings.Add(new CalendarBookingViewModel
                        {
                            Id = booking.Id,
                            Unit = booking.UnitId
                        });
                    }
                    else if (booking.Start <= date.Date && booking.Start.AddDays(booking.Nights).AddDays(rental.PreparationTimeInDays) > date.Date)
                        date.PreparationTimes.Add(new PreparationTimeViewModel
                        {
                            Unit = booking.UnitId
                        });
                }

                result.Dates.Add(date);
            }

            return result;
        }
    }
}
