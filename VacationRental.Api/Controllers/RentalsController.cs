﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VacationRental.Api.Models;
using VacationRental.Business.DTO;
using VacationRental.Business.Interfaces;

namespace VacationRental.Api.Controllers
{
    [Route("api/v1/rentals")]
    [ApiController]
    public class RentalsController : ControllerBase
    {
        private readonly IRentalService rentalService;

        public RentalsController(IRentalService rentalService)
        {
            this.rentalService = rentalService;
        }

        [HttpGet]
        [Route("{rentalId:int}")]
        public async Task<RentalViewModel> Get(int rentalId)
        {
            if (!await rentalService.IsARegisteredUserRentalByIdAsync(rentalId))
                throw new ApplicationException("Rental not found");

            var rental = await rentalService.GetRegisteredUserRentalByIdAsync(rentalId);

            return new RentalViewModel
            {
                Id = rental.Id,
                Units = rental.NumberOfUnits,
                PreparationTimeInDays = rental.PreparationTimeInDays
            };
        }

        [HttpPost]
        public async Task<ResourceIdViewModel> Post(RentalBindingModel model)
        {
            var rentalId = await rentalService.RegisterUserRentalAsync(new RentalDTO
            {
                NumberOfUnits = model.Units,
                PreparationTimeInDays = model.PreparationTimeInDays
            });

            return new ResourceIdViewModel { Id = rentalId };
        }

        [HttpPut]
        [Route("{rentalId:int}")]
        public async Task<ResourceIdViewModel> Put(int rentalId, RentalBindingModel model)
        {
            try
            {
                await rentalService.TryUpdateUserRentalAsync(new RentalDTO
                {
                    Id = rentalId,
                    NumberOfUnits = model.Units,
                    PreparationTimeInDays = model.PreparationTimeInDays
                });
            }
            catch (InvalidOperationException ex)
            {
                throw new ApplicationException(ex.Message);
            }

            return new ResourceIdViewModel { Id = rentalId };
        }
    }
}