﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using VacationRental.Business.DTO;
using VacationRental.Business.Services;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business.UnitTests
{
    [TestFixture]
    public class BookingServiceTests
    {
        private BookingService target;
        private Mock<IBookingRepository> bookingRepository;
        private Mock<IBookingStrategy> bookingStrategy;

        [SetUp]
        public void SetUp()
        {
            bookingRepository = new Mock<IBookingRepository>();
            bookingStrategy = new Mock<IBookingStrategy>();
            target = new BookingService(bookingRepository.Object, bookingStrategy.Object);
        }

        [Test]
        public async Task GetBookingsByRentalIdAsync_ShouldReturnCorrectElements()
        {
            bookingRepository.Setup(x => x.GetBookingsByRentalIdAsync(It.IsAny<int>()))
                .ReturnsAsync(new List<Booking>
                {
                    new Booking
                    {
                        Id = 55,
                        UnitId = 13,
                        Unit = new Unit {Id = 13, RentalId = 22},
                        Start = new DateTime(1984, 10, 11),
                        Nights = 4
                    }
                });

            var result = await target.GetBookingsByRentalIdAsync(55);

            result.Count.Should().Be(1);
            result.First().Id.Should().Be(55);
            result.First().Start.Should().Be(new DateTime(1984, 10, 11));
            result.First().Nights.Should().Be(4);
            result.First().RentalId.Should().Be(22);
            result.First().UnitId.Should().Be(13);
        }

        [Test]
        public async Task GetBookingInfoByIdAsync_ShouldReturnCorrectElement()
        {
            bookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(new Booking
                {
                    Id = 55,
                    Unit = new Unit { RentalId = 22 },
                    Start = new DateTime(1984, 10, 11),
                    Nights = 4
                });

            var result = await target.GetBookingInfoByIdAsync(11);

            result.Id.Should().Be(55);
            result.RentalId.Should().Be(22);
            result.Start.Should().Be(new DateTime(1984, 10, 11));
            result.Nights.Should().Be(4);
        }

        [Test]
        public async Task SubmitBookingForRentalAsync_ShouldAddCorrectElementAndReturnId()
        {
            Booking booking = default;
            var unit = new Unit { Id = 33 };

            bookingStrategy.Setup(x => x.IsAvailableAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .ReturnsAsync(true);
            bookingStrategy.Setup(x => x.GetFreeUnitByRentalIdAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .ReturnsAsync(unit);
            bookingRepository.Setup(x => x.AddAsync(It.IsAny<Booking>()))
                .ReturnsAsync(23)
                .Callback((Booking x) => booking = x);

            var result = await target.SubmitBookingForRentalAsync(new BookingDTO
            {
                RentalId = 11,
                Start = new DateTime(1984, 10, 11),
                Nights = 6
            });

            bookingRepository.Verify(x => x.AddAsync(It.IsAny<Booking>()), Times.Once);
            result.Should().Be(23);
            booking.UnitId.Should().Be(33);
            booking.Unit.Should().Be(unit);
            booking.Start.Should().Be(new DateTime(1984, 10, 11));
            booking.Nights.Should().Be(6);
        }

        [Test]
        public void SubmitBookingForRentalAsync_ShouldThrow_IfRentalNotAvailable()
        {
            bookingStrategy.Setup(x => x.IsAvailableAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .ReturnsAsync(false);

            Assert.ThrowsAsync<InvalidOperationException>(async () => await target.SubmitBookingForRentalAsync(new BookingDTO()));
        }

        [Test]
        public void SubmitBookingForRentalAsync_ShouldThrow_IfStrategyThrows()
        {
            bookingStrategy.Setup(x => x.IsAvailableAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .ReturnsAsync(true);
            bookingStrategy.Setup(x => x.GetFreeUnitByRentalIdAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .ThrowsAsync(new InvalidOperationException());

            Assert.ThrowsAsync<InvalidOperationException>(async () => await target.SubmitBookingForRentalAsync(new BookingDTO()));
        }
    }
}
