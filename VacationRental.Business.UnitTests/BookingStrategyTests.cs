﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business.UnitTests
{
    [TestFixture]
    public class BookingStrategyTests
    {
        private NotOverlappingBookingStrategy notOverlappingBookingStrategy;
        private BookingClosedStrategy bookingClosedStrategy;
        private Mock<IRentalRepository> rentalRepository;

        [SetUp]
        public void SetUp()
        {
            rentalRepository = new Mock<IRentalRepository>();
            notOverlappingBookingStrategy = new NotOverlappingBookingStrategy(rentalRepository.Object);
            bookingClosedStrategy = new BookingClosedStrategy();
        }

        [TestCase(15, 1)]
        [TestCase(15, 5)]
        [TestCase(14, 1)]
        [TestCase(14, 5)]
        [TestCase(10, 1)]
        [TestCase(10, 5)]
        [TestCase(9, 1)]
        [TestCase(9, 5)]
        [TestCase(5, 5)]
        [TestCase(5, 6)]
        [TestCase(5, 10)]
        [TestCase(1, 10)]
        [TestCase(1, 11)]

        public async Task NotOverlappingBookingStrategy_IsAvailableAsync_ShouldReturnFalse_IfOverlappingBooking(int onDay, int nights)
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(CreateRentalWithUnitsOccupiedOnSpecificDay(5, 5, 10, 5, 3));

            var result = await notOverlappingBookingStrategy.IsAvailableAsync(1, OnDay(onDay), nights);

            result.Should().BeFalse();
        }

        [TestCase(17, 1)]
        [TestCase(17, 5)]
        [TestCase(17, 1)]
        [TestCase(17, 5)]
        [TestCase(16, 1)]
        [TestCase(16, 5)]
        [TestCase(7, 2)]
        [TestCase(7, 1)]
        [TestCase(5, 4)]
        [TestCase(5, 4)]
        [TestCase(5, 3)]
        [TestCase(1, 8)]
        [TestCase(1, 7)]

        public async Task NotOverlappingBookingStrategy_IsAvailableAsync_ShouldReturnFalse_IfOnPreparationDay(int onDay, int nights)
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(CreateRentalWithUnitsOccupiedOnSpecificDay(5, 5, 10, 5, 3));

            var result = await notOverlappingBookingStrategy.IsAvailableAsync(1, OnDay(onDay), nights);

            result.Should().BeFalse();
        }

        [TestCase(18, 1)]
        [TestCase(18, 5)]
        [TestCase(6, 1)]
        [TestCase(5, 2)]
        [TestCase(5, 1)]
        [TestCase(1, 6)]
        [TestCase(1, 5)]
        public async Task NotOverlappingBookingStrategy_IsAvailableAsync_ShouldReturnTrue(int onDay, int nights)
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(CreateRentalWithUnitsOccupiedOnSpecificDay(5, 5, 10, 5, 3));

            var result = await notOverlappingBookingStrategy.IsAvailableAsync(1, OnDay(onDay), nights);

            result.Should().BeTrue();
        }

        [Test]
        public async Task NotOverlappingBookingStrategy_GetFreeUnitByRentalIdAsync_ShouldReturnFirstAvailableUnit()
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(CreateRentalWithUnitsOccupiedOnSpecificDay(5, 4, 10, 5, 3));

            var result = await notOverlappingBookingStrategy.GetFreeUnitByRentalIdAsync(1, OnDay(10), 5);

            result.Id.Should().Be(5);
            result.RentalId.Should().Be(1);

            result = await notOverlappingBookingStrategy.GetFreeUnitByRentalIdAsync(1, OnDay(18), 5);

            result.Id.Should().Be(1);
            result.RentalId.Should().Be(1);
        }

        [Test]
        public void NotOverlappingBookingStrategy_GetFreeUnitByRentalIdAsync_ShouldThrow_IfOverlappingBooking()
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(CreateRentalWithUnitsOccupiedOnSpecificDay(5, 5, 10, 5, 3));

            Assert.ThrowsAsync<InvalidOperationException>(async () => await notOverlappingBookingStrategy.GetFreeUnitByRentalIdAsync(1, OnDay(10), 5));
        }

        [TestCase(1, 1, true)]
        [TestCase(1, 2, true)]
        [TestCase(1, 3, true)]
        [TestCase(3, 3, true)]
        [TestCase(2, 3, true)]
        [TestCase(3, 2, true)]
        [TestCase(3, 1, true)]
        [TestCase(1, 4, false)]
        [TestCase(1, 4, false)]
        [TestCase(1, 5, false)]
        [TestCase(3, 5, false)]
        [TestCase(2, 6, false)]
        [TestCase(3, 6, false)]
        [TestCase(3, 10, false)]
        public async Task NotOverlappingBookingStrategy_IsValidUpdateAsync_ShouldReturnCorrectValueForPreparationDays(int oldPreparationTimeInDays, int newPreparationTimeInDays, bool expected)
        {
            var oldRental = new Rental
            {
                Id = 1,
                PreparationTimeInDays = oldPreparationTimeInDays,
                Units = new List<Unit>()
            };
            for (var i = 1; i <= 10; i++)
                oldRental.Units.Add(new Unit
                {
                    Id = i,
                    RentalId = oldRental.Id,
                    Rental = oldRental,
                    Bookings = new List<Booking>()
                });
            for (var i = 0; i < 10; i++)
            {
                oldRental.Units[i].Bookings.Add(new Booking
                {
                    Start = OnDay(1),
                    Nights = 1
                });

                oldRental.Units[i].Bookings.Add(new Booking
                {
                    Start = OnDay(5),
                    Nights = 1
                });

                oldRental.Units[i].Bookings.Add(new Booking
                {
                    Start = OnDay(15),
                    Nights = 1
                });
            }

            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(oldRental);

            var result = await notOverlappingBookingStrategy.IsValidUpdateAsync(1, 10, newPreparationTimeInDays);

            result.Should().Be(expected);
        }

        [TestCase(10, 11, true)]
        [TestCase(10, 9, true)]
        [TestCase(10, 8, true)]
        [TestCase(5, 3, true)]
        [TestCase(5, 10, true)]
        [TestCase(3, 3, true)]
        [TestCase(3, 1, true)]
        [TestCase(10, 7, false)]
        [TestCase(10, 6, false)]
        [TestCase(10, 5, false)]
        [TestCase(5, 2, false)]
        [TestCase(5, 0, false)]
        [TestCase(3, 0, false)]
        [TestCase(3, -1, false)]
        public async Task NotOverlappingBookingStrategy_IsValidUpdateAsync_ShouldReturnCorrectValueForUnits(int oldNumberOfUnits, int newNumberOfUnits, bool expected)
        {
            var oldRental = new Rental
            {
                Id = 1,
                PreparationTimeInDays = 1,
                Units = new List<Unit>()
            };
            for (var i = 1; i <= oldNumberOfUnits; i++)
                oldRental.Units.Add(new Unit
                {
                    Id = i,
                    RentalId = oldRental.Id,
                    Rental = oldRental,
                    Bookings = new List<Booking>()
                });
            for (var i = 0; i < oldNumberOfUnits - 2; i++)
            {
                oldRental.Units[i].Bookings.Add(new Booking
                {
                    Start = OnDay(1),
                    Nights = 1,
                    UnitId = oldRental.Units[i].Id,
                    Unit = oldRental.Units[i]
                });
            }

            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(oldRental);

            var result = await notOverlappingBookingStrategy.IsValidUpdateAsync(1, newNumberOfUnits, 1);

            result.Should().Be(expected);
        }

        [Test]
        public async Task BookingClosedStrategy_IsAvailableAsync_ShouldReturnFalse()
        {
            var result = await bookingClosedStrategy.IsAvailableAsync(1, OnDay(1), 1);

            result.Should().BeFalse();
        }

        [Test]
        public void BookingClosedStrategy_GetFreeUnitByRentalIdAsync_ShouldThrow()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await bookingClosedStrategy.GetFreeUnitByRentalIdAsync(1, OnDay(1), 1));
        }

        [Test]
        public async Task BookingClosedStrategy_IsValidUpdateAsync_ShouldReturnFalse()
        {
            var result = await bookingClosedStrategy.IsValidUpdateAsync(1, 1, 1);

            result.Should().BeFalse();
        }

        private static DateTime OnDay(int day)
        {
            var baseDate = new DateTime(1984, 10, 11);

            return baseDate.AddDays(day - 1);
        }

        private static Rental CreateRentalWithUnitsOccupiedOnSpecificDay(int units, int occupiedUnits, int day, int nights, int preparationTimeInDays)
        {
            var rental = new Rental
            {
                Id = 1,
                PreparationTimeInDays = preparationTimeInDays,
                Units = new List<Unit>()
            };

            for (var i = 1; i <= units; i++)
                rental.Units.Add(new Unit
                {
                    Id = i,
                    RentalId = rental.Id,
                    Rental = rental,
                    Bookings = new List<Booking>()
                });

            for (var i = 0; i < occupiedUnits; i++)
            {
                rental.Units[i].Bookings.Add(new Booking
                {
                    Id = i + 1,
                    Start = OnDay(day),
                    Nights = nights,
                    UnitId = rental.Units[i].Id,
                    Unit = rental.Units[i]
                });
            }

            return rental;
        }
    }
}
