﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using VacationRental.Business.DTO;
using VacationRental.Business.Services;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business.UnitTests
{
    [TestFixture]
    public class RentalServiceTests
    {
        private RentalService target;
        private Mock<IRentalRepository> rentalRepository;
        private Mock<IBookingStrategy> bookingStrategy;

        [SetUp]
        public void SetUp()
        {
            rentalRepository = new Mock<IRentalRepository>();
            bookingStrategy = new Mock<IBookingStrategy>();
            target = new RentalService(rentalRepository.Object, bookingStrategy.Object);
        }

        [Test]
        public async Task IsARegisteredUserRentalByIdAsync_ShouldReturnCorrectResult([Values(true, false)] bool expected)
        {
            rentalRepository.Setup(x => x.IsExistingByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(expected);

            var result = await target.IsARegisteredUserRentalByIdAsync(5);

            result.Should().Be(expected);
        }

        [Test]
        public async Task GetRegisteredUserRentalByIdAsync_ShouldReturnCorrectElement()
        {
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(new Rental
                {
                    Id = 1,
                    PreparationTimeInDays = 2,
                    Units = new List<Unit>
                    {
                        new Unit(),
                        new Unit(),
                        new Unit(),
                        new Unit()
                    }
                });

            var result = await target.GetRegisteredUserRentalByIdAsync(22);

            result.Id.Should().Be(1);
            result.PreparationTimeInDays.Should().Be(2);
            result.NumberOfUnits.Should().Be(4);
        }

        [Test]
        public async Task RegisterUserRentalAsync_ShouldAddCorrectElementAndReturnId()
        {
            Rental rental = default;

            rentalRepository.Setup(x => x.AddAsync(It.IsAny<Rental>()))
                .ReturnsAsync(23)
                .Callback((Rental x) => rental = x);

            var result = await target.RegisterUserRentalAsync(new RentalDTO
            {
                NumberOfUnits = 2,
                PreparationTimeInDays = 3
            });

            rentalRepository.Verify(x => x.AddAsync(It.IsAny<Rental>()), Times.Once);
            result.Should().Be(23);
            rental.Units.Count.Should().Be(2);
            rental.PreparationTimeInDays.Should().Be(3);
        }

        [Test]
        public void TryUpdateUserRentalAsync_ShouldThrow_IfPolicyViolation()
        {
            bookingStrategy.Setup(x => x.IsValidUpdateAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(false);

            Assert.ThrowsAsync<InvalidOperationException>(async () => await target.TryUpdateUserRentalAsync(new RentalDTO()));
        }

        [TestCase(6, 6, 6)]
        [TestCase(6, 6, 0)]
        [TestCase(6, 5, 5)]
        [TestCase(5, 4, 1)]
        [TestCase(5, 4, 0)]
        [TestCase(5, 10, 5)]
        [TestCase(5, 10, 1)]
        [TestCase(10, 20, 10)]
        [TestCase(10, 20, 0)]
        [TestCase(10, 20, 8)]
        public async Task TryUpdateUserRentalAsync_ShouldCallRepositoryWithCorrectElement_IfNoViolation(int newNumberOfUnits, int oldNumberOfUnits, int numberOfBookedUnits)
        {
            Rental newRental = default;
            var oldRental = new Rental
            {
                Id = 1,
                PreparationTimeInDays = 22,
                Units = new List<Unit>()
            };
            for (var i = 1; i <= oldNumberOfUnits; i++)
                oldRental.Units.Add(new Unit
                {
                    Id = i,
                    RentalId = oldRental.Id,
                    Rental = oldRental,
                    Bookings = new List<Booking>()
                });
            for (var i = 0; i < numberOfBookedUnits; i++)
                oldRental.Units[i].Bookings.Add(new Booking());

            bookingStrategy.Setup(x => x.IsValidUpdateAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(true);
            rentalRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(oldRental);
            rentalRepository.Setup(x => x.UpdateAsync(It.IsAny<Rental>()))
                .Callback((Rental x) => newRental = x);

            await target.TryUpdateUserRentalAsync(new RentalDTO
            {
                Id = 1,
                NumberOfUnits = newNumberOfUnits,
                PreparationTimeInDays = 33
            });

            newRental.Id.Should().Be(1);
            newRental.PreparationTimeInDays.Should().Be(33);
            newRental.Units.Count.Should().Be(newNumberOfUnits);
            newRental.Units.Count(x => x.Bookings != null && x.Bookings.Any()).Should().Be(numberOfBookedUnits);
        }
    }
}
