﻿using System;
using System.Linq;
using System.Threading.Tasks;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business
{
    /// <summary>
    /// Implements this interface to have different policy to be checked during the search of an available rental
    /// </summary>
    public interface IBookingStrategy
    {
        Task<bool> IsAvailableAsync(int rentalId, DateTime start, int nights);

        Task<Unit> GetFreeUnitByRentalIdAsync(int rentalId, DateTime start, int nights);

        Task<bool> IsValidUpdateAsync(int rentalId, int numberOfUnits, int preparationTimeInDays);
    }

    /// <summary>
    /// Strategy for not overlapping booking
    ///
    /// This is the strategy required by the assignment
    /// </summary>
    internal class NotOverlappingBookingStrategy : IBookingStrategy
    {
        private readonly IRentalRepository rentalRepository;

        public NotOverlappingBookingStrategy(IRentalRepository rentalRepository)
        {
            this.rentalRepository = rentalRepository;
        }

        /// <summary>
        /// Count the number of units with an overlapping booking
        /// If this number is equal to the number of unit, then the whole rental is occupied
        /// </summary>
        public async Task<bool> IsAvailableAsync(int rentalId, DateTime start, int nights)
        {
            var rental = await rentalRepository.GetByIdAsync(rentalId);
            var occupiedUnitsNumber = rental.Units
                .Count(x => x.Bookings.Any(y => IsOverlappingBooking(y, start, nights, rental.PreparationTimeInDays)));

            return occupiedUnitsNumber < rental.Units.Count;
        }

        /// <summary>
        /// Get the id of the first unit without an overlapping booking
        /// </summary>
        public async Task<Unit> GetFreeUnitByRentalIdAsync(int rentalId, DateTime start, int nights)
        {
            var rental = await rentalRepository.GetByIdAsync(rentalId);

            // A preliminary check has been done in the service layer. This is why we don't use FirstOfDefault
            return rental.Units
                .First(x => !x.Bookings.Any(y => IsOverlappingBooking(y, start, nights, rental.PreparationTimeInDays)));
        }

        /// <summary>
        /// Check if a configuration for an existing rental is valid
        ///
        /// The violations happen if we have future bookings that violate these constraints:
        ///     - If preparation time is greater then we have overlapping with existing bookings
        ///     - If number of units are less, we could have bookings on a unit we are going to eliminate
        ///
        /// We don't allow relocation (e.g. moving an end customer from a booked unit to another different unit)
        /// </summary>
        public async Task<bool> IsValidUpdateAsync(int rentalId, int numberOfUnits, int preparationTimeInDays)
        {
            var rental = await rentalRepository.GetByIdAsync(rentalId);

            if (rental.PreparationTimeInDays < preparationTimeInDays)
            {
                foreach (var unit in rental.Units)
                {
                    var orderedBookings = unit.Bookings.OrderBy(x => x.Start).ToList();

                    for (var i = 1; i < orderedBookings.Count; i++)
                    {
                        var currentBooking = orderedBookings[i];
                        var previousBooking = orderedBookings[i - 1];

                        if (IsOverlappingBooking(previousBooking, currentBooking.Start, currentBooking.Nights, preparationTimeInDays))
                            return false;
                    }
                }
            }

            if (rental.Units.Count > numberOfUnits)
            {
                // Count the number of units that already had at least a booking
                if (rental.Units.SelectMany(x => x.Bookings).Select(x => x.Unit).Distinct().Count() > numberOfUnits)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Check if a booking overlaps with a specific request starting on a specific date for n nights
        /// It takes in account the preparation time for the rental
        ///
        /// All dates are supposed to be saved and sent in UTC (timezone should be considered in a worldwide environment) 
        /// </summary>
        private static bool IsOverlappingBooking(Booking booking, DateTime start, int nights, int preparationTimeInDays)
        {
            // The start falls when there is already a booking or a preparation
            if (start >= booking.Start && start <= booking.Start.AddDays(booking.Nights - 1).AddDays(preparationTimeInDays))
                return true;

            // The end + preparation falls when there is a booking already started
            if (start <= booking.Start && start.AddDays(nights - 1).AddDays(preparationTimeInDays) >= booking.Start)
                return true;

            return false;
        }
    }

    /// <summary>
    /// Strategy for booking closed
    ///
    /// We can easily stop all booking by switching to this strategy during the service registration
    /// </summary>
    internal class BookingClosedStrategy : IBookingStrategy
    {
        public Task<bool> IsAvailableAsync(int rentalId, DateTime start, int nights) => Task.FromResult(false);

        public Task<Unit> GetFreeUnitByRentalIdAsync(int rentalId, DateTime start, int nights)
        {
            throw new InvalidOperationException("This rental is not available");
        }

        public Task<bool> IsValidUpdateAsync(int rentalId, int numberOfUnits, int preparationTimeInDays) => Task.FromResult(false);
    }
}