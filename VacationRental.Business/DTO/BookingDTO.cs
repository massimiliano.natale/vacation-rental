﻿using System;
using VacationRental.Business.Interfaces;

namespace VacationRental.Business.DTO
{
    public class BookingDTO : IDTO
    {
        public int Id { get; set; }
        public int RentalId { get; set; }
        public int UnitId { get; set; }
        public DateTime Start { get; set; }
        public int Nights { get; set; }
    }
}
