﻿using VacationRental.Business.Interfaces;

namespace VacationRental.Business.DTO
{
    public class RentalDTO : IDTO
    {
        public int Id { get; set; }
        public int NumberOfUnits { get; set; }
        public int PreparationTimeInDays { get; set; }
    }
}
