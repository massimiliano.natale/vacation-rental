﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VacationRental.Business.DTO;

namespace VacationRental.Business.Interfaces
{
    public interface IBookingService
    {
        Task<List<BookingDTO>> GetBookingsByRentalIdAsync(int rentalId);

        Task<BookingDTO> GetBookingInfoByIdAsync(int id);

        Task<int> SubmitBookingForRentalAsync(BookingDTO booking);
    }
}