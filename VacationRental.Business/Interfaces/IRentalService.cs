﻿using System.Threading.Tasks;
using VacationRental.Business.DTO;

namespace VacationRental.Business.Interfaces
{
    public interface IRentalService
    {
        Task<bool> IsARegisteredUserRentalByIdAsync(int id);

        Task<RentalDTO> GetRegisteredUserRentalByIdAsync(int id);

        Task<int> RegisterUserRentalAsync(RentalDTO rental);

        Task TryUpdateUserRentalAsync(RentalDTO rental);
    }
}