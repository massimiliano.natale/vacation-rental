﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;
using VacationRental.Business.Interfaces;
using VacationRental.Business.Services;
using VacationRental.DataAccess;

[assembly: InternalsVisibleTo("VacationRental.Business.UnitTests")]
namespace VacationRental.Business
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterInternalDependencies(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddSingleton<IRentalService, RentalService>()
                .AddSingleton<IBookingService, BookingService>()
                .AddSingleton<IBookingStrategy, NotOverlappingBookingStrategy>()
                .AddRepositories();
        }
    }
}