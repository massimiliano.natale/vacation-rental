﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationRental.Business.DTO;
using VacationRental.Business.Interfaces;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business.Services
{
    internal class BookingService : IBookingService
    {
        private readonly IBookingRepository bookingRepository;
        private readonly IBookingStrategy bookingStrategy;

        public BookingService(IBookingRepository bookingRepository, IBookingStrategy bookingStrategy)
        {
            this.bookingRepository = bookingRepository;
            this.bookingStrategy = bookingStrategy;
        }


        /// <summary>
        /// Get all bookings for a rental
        /// </summary>
        /// <param name="rentalId">Id of the rental</param>
        /// <returns>All the found bookings</returns>
        public async Task<List<BookingDTO>> GetBookingsByRentalIdAsync(int rentalId)
        {
            var bookings = await bookingRepository.GetBookingsByRentalIdAsync(rentalId);

            return bookings.Select(x => new BookingDTO
            {
                Id = x.Id,
                Start = x.Start,
                Nights = x.Nights,
                RentalId = x.Unit.RentalId,
                UnitId = x.UnitId
            }).ToList();
        }

        /// <summary>
        /// Get info related to a specific booking
        /// </summary>
        /// <param name="id">Id of the booking</param>
        /// <returns>Selected booking</returns>
        public async Task<BookingDTO> GetBookingInfoByIdAsync(int id)
        {
            var booking = await bookingRepository.GetByIdAsync(id);

            return new BookingDTO
            {
                Id = booking.Id,
                RentalId = booking.Unit.RentalId,
                Start = booking.Start,
                Nights = booking.Nights
            };
        }

        /// <summary>
        /// Try to submit the booking for an end user
        ///
        /// Check for rental availability is done at this level
        /// </summary>
        /// <param name="booking">Booking to be submitted</param>
        public async Task<int> SubmitBookingForRentalAsync(BookingDTO booking)
        {
            if (!await bookingStrategy.IsAvailableAsync(booking.RentalId, booking.Start, booking.Nights))
                throw new InvalidOperationException("This rental is not available");

            // Improvements: This should implemented as a business transaction
            // In a multi threading environment these 2 following calls should be atomic (not done for question of timing)

            var unit = await bookingStrategy.GetFreeUnitByRentalIdAsync(booking.RentalId, booking.Start, booking.Nights);

            return await bookingRepository.AddAsync(new Booking
            {
                Start = booking.Start,
                Nights = booking.Nights,
                UnitId = unit.Id,
                Unit = unit
            });
        }
    }
}
