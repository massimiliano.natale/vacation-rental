﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationRental.Business.DTO;
using VacationRental.Business.Interfaces;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.Business.Services
{
    internal class RentalService : IRentalService
    {
        private readonly IRentalRepository rentalRepository;
        private readonly IBookingStrategy bookingStrategy;

        public RentalService(IRentalRepository rentalRepository, IBookingStrategy bookingStrategy)
        {
            this.rentalRepository = rentalRepository;
            this.bookingStrategy = bookingStrategy;
        }

        /// <summary>
        /// Verify if a rental is registered for a B2B customer
        /// </summary>
        /// <param name="id">Id of the rental</param>
        /// <returns>True if the rental is found</returns>
        public Task<bool> IsARegisteredUserRentalByIdAsync(int id) => rentalRepository.IsExistingByIdAsync(id);

        /// <summary>
        /// Get info related to a registered rental for a B2B customer
        /// </summary>
        /// <param name="id">Id of the rental</param>
        /// <returns>Selected rental</returns>
        public async Task<RentalDTO> GetRegisteredUserRentalByIdAsync(int id)
        {
            var rental = await rentalRepository.GetByIdAsync(id);

            return new RentalDTO
            {
                Id = rental.Id,
                NumberOfUnits = rental.Units.Count,
                PreparationTimeInDays = rental.PreparationTimeInDays
            };
        }

        /// <summary>
        /// Register a new rental for a B2B customer
        ///
        /// Units are created at this level and associated to the related rental
        /// We don't expose Units to external as these are used internally to deal with reservations
        /// </summary>
        /// <param name="rental">Rental to be registered in our system</param>
        /// <returns>Id of the new registered rental</returns>
        public Task<int> RegisterUserRentalAsync(RentalDTO rental)
        {
            var newRental = new Rental
            {
                PreparationTimeInDays = rental.PreparationTimeInDays,
                Units = new List<Unit>()
            };

            for (var i = 0; i < rental.NumberOfUnits; i++)
                newRental.Units.Add(new Unit());

            return rentalRepository.AddAsync(newRental);
        }

        /// <summary>
        /// Try to update units or preparation time for an existing user rental
        ///
        /// It throws if the new configuration violates the business rules defined by the selected booking strategy
        /// </summary>
        /// <param name="rental">Updated user rental configuration</param>
        public async Task TryUpdateUserRentalAsync(RentalDTO rental)
        {
            if (!await bookingStrategy.IsValidUpdateAsync(rental.Id, rental.NumberOfUnits, rental.PreparationTimeInDays))
                throw new InvalidOperationException("This update is violating the booking constraints");

            var oldRental = await rentalRepository.GetByIdAsync(rental.Id);
            var updatedRental = new Rental
            {
                Id = rental.Id,
                PreparationTimeInDays = rental.PreparationTimeInDays,
                Units = oldRental.Units
            };

            // If the number of units are less, keep units with bookings
            if (rental.NumberOfUnits < oldRental.Units.Count)
            {
                var unitsWithNoBooking = updatedRental.Units.Where(x => x.Bookings == null || !x.Bookings.Any());
                unitsWithNoBooking = unitsWithNoBooking.Take(oldRental.Units.Count - rental.NumberOfUnits);

                updatedRental.Units.RemoveAll(x => unitsWithNoBooking.Contains(x));
            }

            // If the number of units are greater, just add the missing units
            else if (rental.NumberOfUnits > oldRental.Units.Count)
            {
                var numberOfNewUnits = rental.NumberOfUnits - oldRental.Units.Count;

                for (var i = 0; i < numberOfNewUnits; i++)
                    updatedRental.Units.Add(new Unit());
            }

            await rentalRepository.UpdateAsync(updatedRental);
        }
    }
}
