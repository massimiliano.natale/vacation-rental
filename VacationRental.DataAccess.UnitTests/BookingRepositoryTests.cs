﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Repositories;

namespace VacationRental.DataAccess.UnitTests
{
    [TestFixture]
    public class BookingRepositoryTests
    {
        private BookingRepository target;
        private DataContext context;

        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "VacationRentalTestDB")
                .Options;

            context = new DataContext(options);
            context.Bookings.Add(new Booking { Id = 1, Unit = new Unit { RentalId = 3 } });
            context.Bookings.Add(new Booking { Id = 2, Unit = new Unit { RentalId = 3 } });
            context.SaveChanges();

            target = new BookingRepository(context);
        }

        [Test]
        public async Task GetBookingsByRentalIdAsync_ShouldReturnCorrectElements()
        {
            await context.Bookings.AddAsync(new Booking
            {
                Id = 4,
                Unit = new Unit { RentalId = 5 }
            });

            await context.Bookings.AddAsync(new Booking
            {
                Id = 5,
                Unit = new Unit { RentalId = 5 }
            });

            await context.Bookings.AddAsync(new Booking
            {
                Id = 6,
                Unit = new Unit { RentalId = 6 }
            });

            await context.SaveChangesAsync();

            var result = (await target.GetBookingsByRentalIdAsync(5)).ToList();

            result.Count.Should().Be(2);
            result.Count(x => x.Unit.RentalId == 5).Should().Be(2);
        }

        [Test]
        public async Task GetByIdAsync_ShouldReturnCorrectElement()
        {
            var result = await target.GetByIdAsync(1);

            result.Id.Should().Be(1);
        }

        [Test]
        public void GetByIdAsync_ShouldThrow_WhenKeyNotFound()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await target.GetByIdAsync(10));
        }

        [Test]
        public async Task AddAsync_ShouldAddElementAndReturnId()
        {
            var result = await target.AddAsync(new Booking { Id = 3 });

            result.Should().Be(3);
            context.Bookings.Count().Should().Be(3);
        }

        [TearDown]
        public void TearDown()
        {
            ClearDatabase();
        }

        private void ClearDatabase()
        {
            context.Bookings.RemoveRange(context.Bookings);
            context.SaveChanges();
        }
    }
}
