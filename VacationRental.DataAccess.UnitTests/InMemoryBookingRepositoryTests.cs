﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Repositories;

namespace VacationRental.DataAccess.UnitTests
{
    [TestFixture]
    public class InMemoryBookingRepositoryTests
    {
        private InMemoryBookingRepository target;
        private ConcurrentDictionary<int, Booking> bookings;


        [SetUp]
        public void SetUp()
        {
            bookings = new ConcurrentDictionary<int, Booking>
            {
                [2] = new Booking { Id = 2, Unit = new Unit { RentalId = 1 } },
                [3] = new Booking { Id = 3, Unit = new Unit { RentalId = 1 } }
            };

            target = new InMemoryBookingRepository(bookings);
        }

        [Test]
        public async Task GetBookingsByRentalIdAsync_ShouldReturnCorrectElements()
        {
            bookings[4] = new Booking
            {
                Id = 4,
                Unit = new Unit { RentalId = 5 }
            };

            bookings[5] = new Booking
            {
                Id = 5,
                Unit = new Unit { RentalId = 5 }
            };

            bookings[6] = new Booking
            {
                Id = 6,
                Unit = new Unit { RentalId = 6 }
            };

            var result = (await target.GetBookingsByRentalIdAsync(5)).ToList();

            result.Count.Should().Be(2);
            result.Count(x => x.Unit.RentalId == 5).Should().Be(2);
        }

        [Test]
        public async Task GetByIdAsync_ShouldReturnCorrectElement()
        {
            var result = await target.GetByIdAsync(2);

            result.Id.Should().Be(2);
        }

        [Test]
        public void GetByIdAsync_ShouldThrow_WhenKeyNotFound()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(async () => await target.GetByIdAsync(10));
        }

        [Test]
        public async Task AddAsync_ShouldAddElementAndReturnId()
        {
            var result = await target.AddAsync(new Booking { Unit = new Unit() });

            result.Should().Be(1);
            bookings[1].Id.Should().Be(1);
            bookings.Count.Should().Be(3);
        }

        [Test]
        public async Task AddAsync_ShouldInitializeRelationship()
        {
            var unit = new Unit { Id = 22 };
            var booking = new Booking { UnitId = unit.Id, Unit = unit };

            await target.AddAsync(booking);

            var result = bookings[1];
            var bookingId = result.Id;

            bookingId.Should().Be(1);
            result.UnitId.Should().Be(22);
            result.Unit.Should().Be(unit);
            result.Unit.Bookings.Count.Should().Be(1);
            result.Unit.Bookings.First().Should().Be(result);
        }
    }
}
