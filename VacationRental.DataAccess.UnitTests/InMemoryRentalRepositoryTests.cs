﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Repositories;

namespace VacationRental.DataAccess.UnitTests
{
    [TestFixture]
    public class InMemoryRentalRepositoryTests
    {
        private InMemoryRentalRepository target;
        private ConcurrentDictionary<int, Rental> rentals;

        [SetUp]
        public void SetUp()
        {
            rentals = new ConcurrentDictionary<int, Rental>
            {
                [2] = new Rental { Id = 2 },
                [3] = new Rental { Id = 3 }
            };

            target = new InMemoryRentalRepository(rentals);
        }

        [TestCase(1, false)]
        [TestCase(2, true)]
        public async Task IsExistingByIdAsync_ShouldReturnCorrectResult(int id, bool expected)
        {
            var result = await target.IsExistingByIdAsync(id);

            result.Should().Be(expected);
        }

        [Test]
        public async Task GetByIdAsync_ShouldReturnCorrectElement()
        {
            var result = await target.GetByIdAsync(2);

            result.Id.Should().Be(2);
        }

        [Test]
        public void GetByIdAsync_ShouldThrow_WhenKeyNotFound()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(async () => await target.GetByIdAsync(10));
        }

        [Test]
        public async Task AddAsync_ShouldAddElementAndReturnId()
        {
            var result = await target.AddAsync(new Rental { Units = new List<Unit>() });

            result.Should().Be(1);
            rentals[1].Id.Should().Be(1);
            rentals.Count.Should().Be(3);
        }

        [Test]
        public async Task AddAsync_ShouldInitializeRelationship()
        {
            var rental = new Rental
            {
                PreparationTimeInDays = 3,
                Units = new List<Unit> { new Unit(), new Unit(), new Unit() }
            };

            await target.AddAsync(rental);

            var result = rentals[1];
            var rentalId = result.Id;

            rentalId.Should().Be(1);
            rental.Units.Count.Should().Be(3);
            foreach (var unit in result.Units)
            {
                unit.Id.Should().BeGreaterThan(0);
                unit.RentalId.Should().Be(rentalId);
            }

            // Verify all created units get a different id
            rental.Units.Select(x => x.Id).Distinct().Count().Should().Be(rental.Units.Count);
        }

        [Test]
        public async Task UpdateAsync_ShouldUpdateCorrectElement()
        {
            var rental = rentals[2];

            rental.PreparationTimeInDays = 10;
            rental.Units = new List<Unit> { new Unit() };

            await target.UpdateAsync(rental);

            rental = rentals[2];
            rental.PreparationTimeInDays.Should().Be(10);
            rental.Units.Count.Should().Be(1);
            rental.Units[0].Bookings.Should().NotBeNull();
        }
    }
}