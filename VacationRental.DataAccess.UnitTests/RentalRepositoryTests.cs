﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Repositories;

namespace VacationRental.DataAccess.UnitTests
{
    [TestFixture]
    public class RentalRepositoryTests
    {
        private RentalRepository target;
        private DataContext context;

        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "VacationRentalTestDB")
                .Options;

            context = new DataContext(options);
            context.Rentals.Add(new Rental { Id = 1 });
            context.Rentals.Add(new Rental { Id = 2 });
            context.SaveChanges();

            target = new RentalRepository(context);
        }

        [TestCase(3, false)]
        [TestCase(2, true)]
        public async Task IsExistingByIdAsync_ShouldReturnCorrectResult(int id, bool expected)
        {
            var result = await target.IsExistingByIdAsync(id);

            result.Should().Be(expected);
        }

        [Test]
        public async Task GetByIdAsync_ShouldReturnCorrectElement()
        {
            var result = await target.GetByIdAsync(1);

            result.Id.Should().Be(1);
        }

        [Test]
        public void GetByIdAsync_ShouldThrow_WhenKeyNotFound()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await target.GetByIdAsync(10));
        }

        [Test]
        public async Task AddAsync_ShouldAddElementAndReturnId()
        {
            var result = await target.AddAsync(new Rental { Id = 3 });

            result.Should().Be(3);
            context.Rentals.Count().Should().Be(3);
        }

        [Test]
        public async Task AddAsync_ShouldInitializeRelationship()
        {
            var rental = new Rental
            {
                PreparationTimeInDays = 3,
                Units = new List<Unit> { new Unit(), new Unit(), new Unit() }
            };

            ClearDatabase();
            await target.AddAsync(rental);

            var result = context.Rentals.First();
            var rentalId = result.Id;

            rentalId.Should().BeGreaterThan(0);
            rental.Units.Count.Should().Be(3);
            foreach (var unit in result.Units)
            {
                unit.Id.Should().BeGreaterThan(0);
                unit.RentalId.Should().Be(rentalId);
            }
        }

        [Test]
        public async Task UpdateAsync_ShouldUpdateCorrectElement()
        {
            var rental = context.Rentals.Single(x => x.Id == 1);

            rental.PreparationTimeInDays = 10;
            rental.Units = new List<Unit> { new Unit() };

            await target.UpdateAsync(rental);

            rental = context.Rentals.Single(x => x.Id == 1);
            rental.PreparationTimeInDays.Should().Be(10);
            rental.Units.Count.Should().Be(1);
        }

        [TearDown]
        public void TearDown()
        {
            ClearDatabase();
        }

        private void ClearDatabase()
        {
            context.Rentals.RemoveRange(context.Rentals);
            context.SaveChanges();
        }
    }
}