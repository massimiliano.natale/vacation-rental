﻿using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;
using VacationRental.DataAccess.Repositories;

[assembly: InternalsVisibleTo("VacationRental.DataAccess.UnitTests")]
namespace VacationRental.DataAccess
{
    public static class DataAccessExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection serviceCollection)
        {
            // Improvements: Connection string and possibility to switch between in-memory and Entity Framework should go in the configuration file
            // This solution has been tested with the in-memory repositories for a question of timing
            // In-memory repositories are not a solution to be used in an enterprise environment

            return serviceCollection
                .AddSingleton(new ConcurrentDictionary<int, Rental>())
                .AddSingleton(new ConcurrentDictionary<int, Booking>())
                .AddDbContext<DataContext>(x => x.UseSqlServer("VacationRentalDB"))
                .AddSingleton<IRentalRepository, InMemoryRentalRepository>()
                .AddSingleton<IBookingRepository, InMemoryBookingRepository>();
        }
    }
}