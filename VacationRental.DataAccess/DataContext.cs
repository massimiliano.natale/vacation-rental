﻿using Microsoft.EntityFrameworkCore;
using VacationRental.DataAccess.Entities;

namespace VacationRental.DataAccess
{
    internal class DataContext : DbContext
    {
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Booking> Bookings { get; set; }

        public DataContext() { }

        public DataContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Rental>()
                .HasMany(x => x.Units)
                .WithOne(x => x.Rental)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Unit>()
                .HasMany(x => x.Bookings)
                .WithOne(x => x.Unit)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Booking>()
                .HasOne(x => x.Unit)
                .WithMany(x => x.Bookings);
        }
    }
}
