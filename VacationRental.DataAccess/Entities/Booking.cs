﻿using System;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Entities
{
    public class Booking : IEntity
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public int Nights { get; set; }

        public int UnitId { get; set; }
        public Unit Unit { get; set; }
    }
}
