﻿using System.Collections.Generic;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Entities
{
    public class Rental : IEntity
    {
        public int Id { get; set; }
        public int PreparationTimeInDays { get; set; }

        public List<Unit> Units { get; set; }
    }
}
