﻿using System.Collections.Generic;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Entities
{
    public class Unit : IEntity
    {
        public int Id { get; set; }

        public int RentalId { get; set; }
        public Rental Rental { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}
