﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VacationRental.DataAccess.Entities;

namespace VacationRental.DataAccess.Interfaces
{
    public interface IBookingRepository
    {
        Task<IEnumerable<Booking>> GetBookingsByRentalIdAsync(int rentalId);

        Task<Booking> GetByIdAsync(int id);

        Task<int> AddAsync(Booking booking);
    }
}