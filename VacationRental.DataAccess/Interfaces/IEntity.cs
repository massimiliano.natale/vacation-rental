﻿namespace VacationRental.DataAccess.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}