﻿using System.Threading.Tasks;
using VacationRental.DataAccess.Entities;

namespace VacationRental.DataAccess.Interfaces
{
    public interface IRentalRepository
    {
        Task<bool> IsExistingByIdAsync(int id);

        Task<Rental> GetByIdAsync(int id);

        Task<int> AddAsync(Rental rental);

        Task UpdateAsync(Rental rental);
    }
}