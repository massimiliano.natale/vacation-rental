﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Repositories
{
    internal class BookingRepository : IBookingRepository
    {
        private readonly DataContext context;

        public BookingRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Booking>> GetBookingsByRentalIdAsync(int rentalId) => await context.Bookings
            .Where(x => x.Unit.RentalId == rentalId)
            .ToListAsync();

        public Task<Booking> GetByIdAsync(int id) => context.Bookings.SingleAsync(x => x.Id == id);

        public async Task<int> AddAsync(Booking booking)
        {
            await context.Bookings.AddAsync(booking);
            await context.SaveChangesAsync();

            return booking.Id;
        }
    }
}
