﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Repositories
{
    internal class InMemoryBookingRepository : IBookingRepository
    {
        private int currentId;
        private readonly ConcurrentDictionary<int, Booking> bookings;

        public InMemoryBookingRepository(ConcurrentDictionary<int, Booking> bookings)
        {
            currentId = 0;
            this.bookings = bookings;
        }

        public Task<IEnumerable<Booking>> GetBookingsByRentalIdAsync(int rentalId) => Task.FromResult(bookings.Values.Where(x => x.Unit.RentalId == rentalId));

        public Task<Booking> GetByIdAsync(int id) => Task.FromResult(bookings[id]);

        /// <summary>
        /// In this method we are replicating what a normal ORM usually does transparently
        /// As we are not leveraging any DB/ORM feature, we need to populate all the ids and navigation properties
        /// </summary>
        /// <param name="booking">Booking to be added</param>
        /// <returns>Id of the added booking</returns>
        public Task<int> AddAsync(Booking booking)
        {
            Interlocked.Increment(ref currentId);

            if (booking.Unit.Bookings == null)
                booking.Unit.Bookings = new List<Booking>();

            booking.Id = currentId;
            booking.Unit.Bookings.Add(booking);
            bookings[currentId] = booking;

            return Task.FromResult(booking.Id);
        }
    }
}
