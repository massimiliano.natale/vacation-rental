﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Repositories
{
    internal class InMemoryRentalRepository : IRentalRepository
    {
        private int currentRentalId;
        private int currentUnitId;
        private readonly ConcurrentDictionary<int, Rental> rentals;

        public InMemoryRentalRepository(ConcurrentDictionary<int, Rental> rentals)
        {
            currentRentalId = 0;
            currentUnitId = 0;
            this.rentals = rentals;
        }

        public Task<bool> IsExistingByIdAsync(int id) => Task.FromResult(rentals.ContainsKey(id));

        public Task<Rental> GetByIdAsync(int id) => Task.FromResult(rentals[id]);

        /// <summary>
        /// In this method we are replicating what a normal ORM usually does transparently
        /// As we are not leveraging any DB/ORM feature, we need to populate all the ids and navigation properties
        /// </summary>
        /// <param name="rental">Rental to be added</param>
        /// <returns>Id of the added rental</returns>
        public Task<int> AddAsync(Rental rental)
        {
            Interlocked.Increment(ref currentRentalId);
            rental.Id = currentRentalId;
            rentals[currentRentalId] = rental;
            foreach (var unit in rental.Units)
            {
                Interlocked.Increment(ref currentUnitId);
                unit.Id = currentUnitId;
                unit.RentalId = rental.Id;
                unit.Rental = rental;
                unit.Bookings = new List<Booking>();
            }

            return Task.FromResult(rental.Id);
        }

        public Task UpdateAsync(Rental rental)
        {
            var addedUnits = rental.Units.Where(x => x.Bookings == null);
            foreach (var unit in addedUnits)
                unit.Bookings = new List<Booking>();

            rentals[rental.Id] = rental;

            return Task.CompletedTask;
        }
    }
}