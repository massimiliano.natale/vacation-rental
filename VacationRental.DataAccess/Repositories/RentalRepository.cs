﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VacationRental.DataAccess.Entities;
using VacationRental.DataAccess.Interfaces;

namespace VacationRental.DataAccess.Repositories
{
    internal class RentalRepository : IRentalRepository
    {
        private readonly DataContext context;

        public RentalRepository(DataContext context)
        {
            this.context = context;
        }

        public Task<bool> IsExistingByIdAsync(int id) => context.Rentals.AnyAsync(x => x.Id == id);

        public Task<Rental> GetByIdAsync(int id) => context.Rentals.SingleAsync(x => x.Id == id);

        public async Task<int> AddAsync(Rental rental)
        {
            await context.Rentals.AddAsync(rental);
            await context.SaveChangesAsync();

            return rental.Id;
        }

        public async Task UpdateAsync(Rental rental)
        {
            context.Rentals.Update(rental);
            await context.SaveChangesAsync();
        }

    }
}